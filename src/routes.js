import Home from './page/Home'

const routes = [
  

    {
        path: "/host",
        component: host,
        private: true,
        routes: [
            {
                path: "/host/Banner",
                component: BannerList,
                private: true
            },
            {
                path: "/host/dashboard",
                component: Dashboard,
                private: true
            },
            {
                path: "/host/calendar",
                component: Calendar,
                private: true
            },
            {
                path: "/host/account-settings",
                component: AccountSettings,
                private: true
            },
            {
                path: "/host/Property",
                component: List,
                private: true
            },
            {
                path: "/host/newProperty",
                component: NewProperty,
                //private: true,
                routes: [
                    {
                        path: "/host/newProperty/slide-1",
                        component: p1,

                    },
                    {
                        path: "/host/newProperty/slide-2",
                        component: p2,

                    },
                    

                ]
            },
        ]

        },


    {
        path: "/404",
        component: Login
    }
]

export default routes;