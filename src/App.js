import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory
} from "react-router-dom";
import { useDispatch } from 'react-redux'
import { gql, useQuery } from '@apollo/client'
import Header from './template/header';
import Footer from './template/Footer';
import './App.css';

import routes from './routes'
import fakeAuth from './fakeAuth';
import RouteWithSubRoutes from './RouteWithSubRoutes'
const SETTINGS = gql`
  query SETTINGS {
    settingsGet {
        siteName

    }
 
  getCurrency{
		name
      icon  
      code
      symbol
      value
  }
}
`;

function App() {
  const { data, loading, error } = useQuery(SETTINGS);

  const dispatch = useDispatch()


  return (
    <Router>
      <Header />
      <Switch>
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
