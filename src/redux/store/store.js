import { createStore, combineReducers, applyMiddleware, compose } from 'redux'

//import headerReducer from '../reducers/header'

// import property from '../reducers/property'
// import BookingReducer from '../reducers/booking'
import thunk from 'redux-thunk';

const Middleware = [thunk]

const allReducers = combineReducers({


})
const store = createStore(allReducers, compose(applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
)

//
export default store