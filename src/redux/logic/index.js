import { connect } from 'react-redux';
import * as ACTION from '../action/'

import store from '../store/store'
export default function dispatch(name, payloadobj) {
    store.dispatch({
        type: ACTION[name], payload: payloadobj
    })
}

