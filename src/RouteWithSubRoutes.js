import React, { useEffect } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory
} from "react-router-dom";
import fakeAuth from './fakeAuth';

function RouteWithSubRoutes(route) {
    let history = useHistory();
    if (route.private == true) {
        if (fakeAuth.isAuthenticated == false) {
            history.push("/404")
        }
    }
    return (
        <Route
            path={route.path}
            render={props => (

                // pass the sub-routes down to keep nesting
                <route.component {...props} routes={route.routes} />
            )}
        />
    );
}


export default RouteWithSubRoutes;