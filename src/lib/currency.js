// import React from 'react';
// import dispatch from '../../../../../redux/logic/'
// const currency = (props) => {
//     let list = ''
// let selectcurrency = (e) => {
//     dispatch('CURRENCY', {
//         currency: e.currencyName,
//         symbel: e.symbel,
//         code: e.code,
//         icon: e.icon

//     })
// }
// if (props.obj) {
//     list = props.obj.map((value, index) => {
//         return <li><a onClick={() => { selectcurrency(value) }}>
//             <i className="icon"> {value.symbel}</i> {value.code}</a></li> //fa-caret-down

//     })
// }
//     return (


//     );
// };

// export default currency;
import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { gql, useQuery, useMutation } from '@apollo/client'


const CURRENCY = gql`
query  {
    getCurrency{
        name
    icon
    symbol
    code
      }
}
  `;




const Currency = () => {
    const ActiveCurrency = useSelector(state => state.coreReducer.currency);
    const dispatch = useDispatch()

    const { loading, error, data } = useQuery(CURRENCY, {
    });


    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    let list = '';


    if (data.getCurrency) {
        list = data.getCurrency.map((value, index) => {
            return <a className="dropdown-item" href="#" onClick={() => { selectcurrency(value) }}>{value.code}</a>
        })
    }

    let selectcurrency = (e) => {

        dispatch({
            type: 'CURRENCY', payload: {
                currency: e.currencyName,
                symbel: e.symbel,
                code: e.code,
                icon: e.icon
            }
        });
    }
    return (
        <div>

            <div className="dropdown">
                <button className="btn btn-buy dropdown-toggle" type="button" id="currencyList" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span> {ActiveCurrency.code} </span>
                </button>
                <div className="dropdown-menu" aria-labelledby="currencyList">
                    {list}
                </div>
            </div>



        </div>
    );
};

export default Currency;