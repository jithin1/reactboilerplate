
// export default currency;
import React from 'react';
import { useSelector, useDispatch } from 'react-redux'

const Language = () => {
    const language = useSelector(state => state.coreReducer.language);
    const dispatch = useDispatch()


    let languageChange = (e) => {

        dispatch({
            type: 'CHANGELANGUAGE', payload: {
                language: e.l,
                name: e.name

            }
        });
    }
    return (

        <div className="dropdown">
            <button className="btn btn-buy dropdown-toggle" type="button" id="language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span> {language.name} </span>
            </button>
            <div className="dropdown-menu" aria-labelledby="language">

                <a className="dropdown-item" href="#" onClick={() => { languageChange({ l: 'ar', name: 'عربى' }) }}> عربى</a>
                <a className="dropdown-item" href="#" onClick={() => { languageChange({ l: 'en', name: 'English' }) }}>English</a>
                <a className="dropdown-item" href="#" onClick={() => { languageChange({ l: 'ml', name: 'മലയാളം' }) }}> മലയാളം</a>
            </div>
        </div>

    );
};

export default Language;