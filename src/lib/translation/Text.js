// /* eslint-disable react/destructuring-assignment */
// /* eslint-disable react/prop-types */
// import React from 'react';
// import { connect } from 'react-redux';
// import Translation from './Translation.jsx';

// class Text extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {};
//   }

//   render() {
//     return (
//       <>
//         <Translation body={this.props.children} lng={this.props.site.language} />
//       </>

//     );
//   }
// }

// const mapStateToProps = (state) => ({
//   site: state.coreReducer.language,

// });

// export default connect(mapStateToProps)(Text);


import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import Translation from './Translation.jsx';

const Text = (props) => {
  const obj = useSelector(state => state.coreReducer.language);
  return (
    <Translation body={props.children} lng={obj.language} />
  );
};

export default Text;