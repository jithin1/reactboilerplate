// /* eslint-disable react/destructuring-assignment */
// /* eslint-disable react/prop-types */
// /* eslint-disable no-useless-constructor */
// /* eslint-disable no-unused-vars */
// /* eslint-disable no-undef */
// // import React from 'react';

// // function Translation(props) {
// //   return (
// //     i18n.t(props.children, { lng: props.lng })
// //   );
// // }

// // export default Translation;

// import React, { Component, Suspense, useState, useEffect, lazy } from "react";
// import { connect } from 'react-redux';
// import i18n from './i18n';

// class Translation extends Component {
//   constructor(props) {
//     super(props);
//   }

//   render() {
//     return (
//       i18n.t(this.props.body, { lng: this.props.site.language })
//     );
//   }
// }

// // export default Translation;

// const mapStateToProps = (state) => ({
//   site: state.coreReducer.language,

// });

// export default connect(mapStateToProps)(Translation);

import React, { Component, Suspense, useState, useEffect, lazy } from "react";
import { useSelector, useDispatch } from 'react-redux'
import i18n from './i18n';

const Translation = (props) => {
  const obj = useSelector(state => state.coreReducer.language);
  return (
    i18n.t(props.body, { lng: obj.language })
  );
};

export default Translation;
